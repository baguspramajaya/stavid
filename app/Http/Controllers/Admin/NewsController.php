<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\News;
use Illuminate\Support\Str;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected function validationData($request){
         $this->validator      = Validator::make(
             $request,
             [
                 'title'          => 'required',
                 'content'            => 'required',
                 'status'            => 'required',
             ],
             [
                 'required'          => ':attribute is required.'
             ],
             [
                 'title'           => 'News Title',
                 'content'             => 'News Content',
                 'status'             => 'Status',
             ]
         );
     }

    public function index(Request $request)
    {
        $name = $request->get('name');

        if ($name) {
            $models = News::where('title', 'like', '%' . $name . '%');
        } else {
            $models = News::orderBy('created_at', 'desc');
        }

        $models = $models->paginate(20);
        return view('admin.news.index',compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
         $this->validationData($request->all());
         if ($this->validator->fails()) {
             return redirect()->back()->withInput($request->all())->withErrors($this->validator->errors());
         }

         $model = new News();
         $model->title = $request->title;
         $model->slug = Str::slug($model->title, '-');
         $model->content = $request->content;
         $model->status = $request->status;
         if($request->file('img') != null){
           $name = $request->img->getClientOriginalName();

           $model->uploadImage($request->file('img'), $name);
           $model->img = $name;
         }
         $model->save();

         return redirect()->route('admin.news.index')->with('info', 'Success add data');
     }
    /**
     * Display the specified resource.
     *
     * @param  \App\News  $News
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $News
     * @return \Illuminate\Http\Response
     */
    public function edit(News $News)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $News
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
         $this->validationData($request->all());
         if ($this->validator->fails()) {
             return redirect()->back()->withInput($request->all())->withErrors($this->validator->errors());
         }

         $model = News::findOrFail($id);
         $model->title = $request->title;
         $model->slug = Str::slug($model->title, '-');
         $model->content = $request->content;
         $model->status = $request->status;
         if($request->file('img') != null){
           $name = $request->img->getClientOriginalName();

           $model->uploadImage($request->file('img'), $name);
           $model->img = $name;
         }

         $model->save();

         return redirect()->route('admin.news.index')->with('info', 'Success update data');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $News
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $model = News::findOrFail($id);
      $model->delete();

      return redirect()->route('admin.news.index')->with('info', 'Success delete data');
    }
}
