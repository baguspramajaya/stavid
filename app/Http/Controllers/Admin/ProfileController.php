<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use Hash;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function show(Dashboard $dashboard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function edit(Dashboard $dashboard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request)
     {
         $id = Auth::user()->id;

         $model = User::findOrFail($id);
         $model->name = $request->name;
         $model->email = $request->email;
         $model->save();

         return redirect()->back()->with('info', 'Success update data');
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  \App\User  $contact
      * @return \Illuminate\Http\Response
      */
      public function password(Request $request)
      {
         if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
             return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
         }

         if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
             return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
         }

         $validatedData = $request->validate([
             'current-password' => 'required',
             'new-password' => 'required|string|min:6|confirmed',
         ]);

         $model = Auth::user();
         $model->password = bcrypt($request->get('new-password'));
         $model->save();

         return redirect()->back()->with("success","Password changed successfully !");
      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dashboard $dashboard)
    {
        //
    }
}
