<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['namespace'=>'User'], function(){
  Route::get('/', 'HomeController@index')->name('index');
  Route::get('/news/{slug}', 'NewsController@index')->name('news-detail');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function () {
  Route::post('/visitor', 'DashboardController@create')->name('visitors');
});

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function () {
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
        Route::resource('/news', 'NewsController', ['names'=>'news']);
        Route::put('/account/update', 'ProfileController@update')->name('account.update');
        Route::post('/account/password', 'ProfileController@password')->name('account.password');
        Route::resource('/setting', 'MetaController', ['names'=>'setting']);
    });
});
Auth::routes();
