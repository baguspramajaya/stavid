<?php

use Illuminate\Database\Seeder;
use App\Meta;

class SiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new User([
            "title"      =>"STAVID",
            "keyword"     =>"covid19, covid, indonesia, corona",
            "description"     =>"Pantau berita terupdate tentang perkembangan virus corona di indonesia",
        ]);
        $user1->save();
    }
}
