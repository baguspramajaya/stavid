@extends('layouts.app')
@section('content')
<div class="form-items">
   <h3>Login to account</h3>
   <p>Access to the most powerfull tool in the entire design and web industry.</p>
   <form method="POST" action="{{ route('login') }}">
      @csrf
      <div class="form-group">
         <div class="input-group">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
            @error('email')
            <span class="invalid-feedback" style="display: block !important;" role="alert">
            <strong>{{ $message }}</strong>
            </span>
            @enderror
         </div>
      </div>
      <div class="form-group">
         <div class="input-group">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
            @error('password')
            <span class="invalid-feedback" style="display: block !important;" role="alert">
            <strong>{{ $message }}</strong>
            </span>
            @enderror
         </div>
      </div>
      <div class="form-button">
         <button id="submit" type="submit" class="ibtn btn-block">Login</button> 
      </div>
   </form>
</div>
@endsection
