<?php
  $models = App\User::where('id',Auth::user()->id)->first();
?>
<div class="modal fade" id="modalSetting" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body pb-5">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="profile-tab" data-toggle="pill" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Profile</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="password-tab" data-toggle="pill" href="#password" role="tab" aria-controls="password" aria-selected="false">Security</a>
          </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <form action="{{route('admin.account.update')}}" method="POST">
              @csrf
              @method('PUT')
              <div class="form-group">
                <label for="" class="col-form-label">Email Address</label>
                <input type="email" class="form-control" value="{{$models->email}}" name="email" placeholder="Email Address">
              </div>
              <div class="form-group">
                <label for="" class="col-form-label">Full Name</label>
                <input type="text" class="form-control" value="{{$models->name}}" name="name" placeholder="Full Name">
              </div>
              <button type="submit" class="btn btn-md btn-primary">Save Changes</button>
            </form>
          </div>
          <div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="password-tab">
            <form action="{{route('admin.account.password')}}" method="POST">
              @csrf
              <div class="form-group">
                <label class="text-muted" for="current-password">Old Password</label>
                <input type="text" class="form-control" id="current-password" aria-describedby="emailHelp" name="current-password" required placeholder="Recent Password">
              </div>
              <div class="form-group">
                <label class="text-muted" for="new-password">New Password</label>
                <input type="text" class="form-control" id="new-password" name="new-password" required placeholder="New Password">
              </div>
              <div class="form-group">
                <label class="text-muted" for="new-password-confirm">New Password Confirm</label>
                <input type="text" class="form-control" id="new-password-confirm" name="new-password_confirmation" required placeholder="New Password">
              </div>
              <button type="submit" class="btn btn-md btn-primary">Change Password</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
